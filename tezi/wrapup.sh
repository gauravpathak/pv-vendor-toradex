#!/bin/sh
#
# (c) Pantacor Ltd 2020
#
# Apalis/Colibri iMX6 in-field hardware update script
#
# One-time configurations (non-reversible!):
# - Fuse SoC to use eMMC Fast Boot mode
# - Enable eMMC H/W reset capabilities
# Required configurations
# - Configure Boot Bus mode (due to eMMC Fast Boot mode above)
#
# Other configurations
# - Boot from eMMC boot partition (must run as wrap-up script)
#

PRODUCT_ID=$1
BOARD_REV=$2
SERIAL=$3
IMAGE_FOLDER=$4

error_exit () {
        echo "$1" 1>&2
        exit 1
}

# Do a basic validation that we do this on one of our modules
case $PRODUCT_ID in
0027|0028|0029|0035)
	sh wrapup.mx6.sh
	;;
0014|0015|0016|0017)
	sh wrapup.mx6.sh
	;;
*)
	echo "wrapup.sh is a no-op on anything but Apalis/Colibri iMX6 Platforms ($PRODUCT_ID). Skipping..."
	echo "Rebooting in 5 seconds ..."
	sleep 5
	poweroff -f
	;;
esac

